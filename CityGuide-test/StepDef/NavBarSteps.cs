﻿using CityGuide_test.BusinessLogic;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace CityGuide_test.StepDef
{
    [Binding]
    public class NavBarSteps
    {
        NavBarBL navBarBL;
        [Given(@"user is on home page")]
        public void GivenUserIsOnHomePage()
        {
            navBarBL = new NavBarBL();
            Assert.IsTrue(navBarBL.verifyTitle("CityGuide"));
        }
        
        [When(@"user click on register option")]
        public void WhenUserClickOnRegisterOption()
        {
            Assert.IsTrue( navBarBL.clickonOpt("Register"));
        }
        
        [Then(@"user is redirected to Register page")]
        public void ThenUserIsRedirectedToRegisterPage()
        {
            Assert.IsTrue(navBarBL.verifyTitle("Register - CityGuide"));
        }


        [When(@"user click on (.*) option")]
        public void WhenUserClickOnOption(string opt)
        {
            if (opt.Equals("Register"))
            {
                Assert.IsTrue(navBarBL.clickonOpt("Register"));
            }
            else if (opt.Equals("Login"))
            {
                Assert.IsTrue(navBarBL.clickonOpt("Login"));
            }
            else
                Assert.IsTrue(false);
        }

        [Then(@"user is redirected to (.*) page")]
        public void ThenUserIsRedirectedToPage(string opt)
        {
            if (opt.Equals("Register"))
            {
                Assert.IsTrue(navBarBL.verifyTitle("Register - CityGuide"));
            }
            else if (opt.Equals("Login"))
            {   
                Assert.IsTrue(navBarBL.verifyTitle("Log in - CityGuide"));
            }
            else
                Assert.IsTrue(false);
        }

    }
}
