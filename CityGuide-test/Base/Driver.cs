﻿using CityGuide_test.CommonUtils;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;

namespace CityGuide_test.Base
{
    public class Driver
    {
        CommonMethods commnutils;
        public static IWebDriver driver;
        public void InvokeDriver()
        {
            commnutils = new CommonMethods();
            driver = new ChromeDriver();
            commnutils.Maximize();
            commnutils.Open_url("https://localhost:44374/");
        }
        public void Close()
        {
            driver.Quit();
        }
    }
}

    