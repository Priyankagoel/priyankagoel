﻿using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace CityGuide_test.Base
{
    [Binding]
    public sealed class Hooks : Driver
    {
        // For additional details on SpecFlow hooks see http://go.specflow.org/doc-hooks

        [BeforeScenario]
        public void BeforeScenario()
        {
            InvokeDriver();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            Close();
        }
    }
}
