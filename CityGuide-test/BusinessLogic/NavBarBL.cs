﻿using CityGuide_test.CommonUtils;
using CityGuide_test.PageObject;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace CityGuide_test.BusinessLogic
{
    class NavBarBL
    {
        CommonMethods commonuitils = new CommonMethods();
        NavBar navBar;
        public bool verifyTitle(String title) {
            
            return commonuitils.Check_title(title);
        }
        public bool clickonOpt(String opt)
        {
            try
            {
                navBar = new NavBar();
                commonuitils.click(findopt(opt));
                return true;
            }
            catch{
                return false;
            }
        }
        public IWebElement findopt(String opt) {
            if (opt.Equals("Register"))
            {
                return navBar.Registeropt();
            }
            else if (opt.Equals("Login"))
            {
                return navBar.Loginopt();
            }
            else return null;
        }
    }
}
