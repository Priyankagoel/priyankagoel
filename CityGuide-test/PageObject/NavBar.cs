﻿using CityGuide_test.Base;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace CityGuide_test.PageObject
{
    class NavBar : Driver
    {
        public IWebElement Registeropt()
        {
            return driver.FindElement(By.Name("Register"));
        }
        public IWebElement Loginopt()
        {
            return driver.FindElement(By.Name("Login"));
        }
    }
}
