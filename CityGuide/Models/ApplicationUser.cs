﻿
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CityGuide.Models
{
    public class ApplicationUser : IdentityUser
    {
        public int Number { get; set; }
        public int age { get; set; }
    }
}
